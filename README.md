# Welcome to the Sky Hub project

For an overview of how the Sky Hub network works please see the diagram on our site: [Network Diagram](https://skyhub.org/assets/Diagram.pdf)


## Tracker Repositories
* [gitlab.com/skyhuborg/tracker](https://gitlab.com/skyhuborg/tracker) - ML, sensor data, video capture & relay to the cloud
* [gitlab.com/skyhuborg/tracker-controller](https://gitlab.com/skyhuborg/tracker-controller) - Backend for the tracker-ui
* [gitlab.com/skyhuborg/tracker-ui](https://gitlab.com/skyhuborg/tracker-ui) - The UI for the tracker

## Portal Repositories

* [gitlab.com/skyhuborg/trackerd](https://gitlab.com/skyhuborg/trackerd) - Server that captures data
* [gitlab.com/skyhuborg/portal-controller](https://gitlab.com/skyhuborg/portal-controller) - Backend for the portal-ui
* [gitlab.com/skyhuborg/portal-ui](https://gitlab.com/skyhuborg/portal-ui) - The UI for the portal

## Proto Repositories - for protobufs

* [gitlab.com/skyhuborg/proto](https://gitlab.com/skyhuborg/proto) - This is the primary repository for all protobuf definitions
* gitlab.com/skyhuborg/proto-* - These repositories are the output generated from proto repo and can be referenced inside code.


If you woud like to get involved, drop into our rocket chat at https://chat.skyhub.org

## Tracker installation

Tracker install is a docker depoyment run on NVidia Jetson hardware.   Instructions can be found in the DEPLOY.md file.